/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import model.Exchange;
import model.Money;

/**
 *
 * @author Andrea
 */
public interface MoneyCalculatorGUI {
    
    public Exchange getExchange();
    
    public void display(Money exchanged);
    
    public void initializeExchangeView();
}
