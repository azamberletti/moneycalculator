/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.swing;

import UI.MoneyCalculatorGUI;
import java.awt.HeadlessException;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import model.CurrencySet;
import model.Exchange;
import model.Money;

/**
 *
 * @author Andrea
 */
public class SwingMoneyCalculatorGUI extends JFrame implements MoneyCalculatorGUI{

    private final CurrencySet currencies;
    private final JPanel panel;
    private final ExchangeDialog dialog;
    private final JButton button;
    private final MoneyDisplay display;
    private ActionListener listener;
    
    public SwingMoneyCalculatorGUI(CurrencySet currencies) throws HeadlessException {
        super();
        this.currencies = currencies;
        this.panel = new JPanel();
        this.setContentPane(this.panel);
        this.dialog = new ExchangeDialog(this.currencies);
        this.button = new JButton("Convert");
        this.display = new MoneyDisplay();
        this.setTitle("Money calculator");
        this.setSize(500,500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    @Override
    public Exchange getExchange() {
        return this.dialog.execute();
    }

    @Override
    public void display(Money exchanged) {
        this.display.update(exchanged);
    }
    
    @Override
    public void initializeExchangeView(){
        this.panel.add(dialog);
        this.panel.add(button);
        this.panel.add(display);
    }
    
    public void setButtonBehaviour(ActionListener listener){
        this.listener = listener;
        this.button.addActionListener(this.listener);
    }
}
