/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.swing;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import model.Currency;
import model.CurrencySet;
import model.Exchange;
import model.Money;

/**
 *
 * @author usuario
 */
public class ExchangeDialog extends JPanel{

    private final JTextField moneyAmount = new JTextField(20);
    private final JComboBox<Currency> from = new JComboBox<>();
    private final JComboBox<Currency> to = new JComboBox<>();
    
    public ExchangeDialog(CurrencySet currencies){
        for(Currency c : currencies.all()){
            from.addItem(c);
            to.addItem(c);
        }
        this.add(moneyAmount);
        this.add(from);
        this.add(to);
    }
    
    public Exchange execute() {
        Double amount = Double.parseDouble(this.moneyAmount.getText());
        Currency fromCurrency = (Currency) from.getSelectedItem();
        Currency toCurrency = (Currency) to.getSelectedItem();
        return new Exchange(new Money(amount, fromCurrency), toCurrency);
    }
}
