/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package process;

import model.Exchange;
import model.ExchangeRate;
import model.Money;

/**
 *
 * @author usuario
 */
public class Exchanger {

    public Exchanger() {
    }

    public Money exchange(Exchange exchange, ExchangeRate rate) {
        return new Money(exchange.getMoney().getAmount()*rate.getRate(),exchange.getToCurrency());
    }
    
}
