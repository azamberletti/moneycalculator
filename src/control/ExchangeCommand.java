/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import process.Exchanger;
import persistence.ExchangeRateLoader;
import model.ExchangeRate;
import model.Exchange;
import UI.MoneyCalculatorGUI;
import model.CurrencySet;
import model.Money;

/**
 *
 * @author usuario
 */
public class ExchangeCommand {

    private Exchange exchange;
    private final CurrencySet currencies;
    private final MoneyCalculatorGUI gui;
    private final ExchangeRateLoader ratesLoader;
    
    public ExchangeCommand(
            MoneyCalculatorGUI gui, CurrencySet currencies, ExchangeRateLoader loader) {
        this.currencies = currencies;
        this.gui = gui;
        this.ratesLoader = loader;
        this.gui.initializeExchangeView();
    }

    public void execute() {
        this.exchange = gui.getExchange();
        ExchangeRate rate = ratesLoader.load(exchange.getMoney().getCurrency(), exchange.getToCurrency());
        Money exchanged = new Exchanger().exchange(exchange, rate);
        gui.display(exchanged);
    }
}
