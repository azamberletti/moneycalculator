/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author usuario
 */
public class ExchangeRate {
    
    private final double rate;

    public ExchangeRate(double rate) {
        this.rate = rate;
    }

    public double getRate() {
        return rate;
    }   
}
