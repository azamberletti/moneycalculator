/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author usuario
 */
public class CurrencySet{
    
    private final static Set<Currency> currencies = new HashSet<>();
    
    public void update(Set<Currency> updated){
        currencies.clear();
        currencies.addAll(updated);
    }
    
    public Currency get(String code){
        Currency matching = null;
        for(Currency c : currencies){
            if(c.getCode().equals(code)){
                matching = c;
            }
        }
        return matching;
    }
    
    public Set<Currency> all(){
        return new HashSet<>(currencies);
    }
}
