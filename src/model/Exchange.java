/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author usuario
 */
public class Exchange {
    
    private final Money money;
    private final Currency toCurrency;

    public Exchange(Money money, Currency toCurrency) {
        this.money = money;
        this.toCurrency = toCurrency;
    }

    public Money getMoney() {
        return money;
    }

    public Currency getToCurrency() {
        return toCurrency;
    }
    
    
}
