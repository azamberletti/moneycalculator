/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mock;

import java.util.HashSet;
import java.util.Set;
import model.Currency;
import model.CurrencySet;
import persistence.CurrencySetLoader;

/**
 *
 * @author Andrea
 */
public class MockCurrencySetLoader implements CurrencySetLoader{

    @Override
    public void load(CurrencySet currencies) {
        Set<Currency> set = new HashSet<>();
        set.add(new Currency("EUR","Euro","€"));
        set.add(new Currency("USD","American Dollar","$"));
        set.add(new Currency("GBP","British Pound","£"));
        currencies.update(set);
    }
    
}
