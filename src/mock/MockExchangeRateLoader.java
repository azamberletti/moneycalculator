/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mock;

import model.Currency;
import model.ExchangeRate;
import persistence.ExchangeRateLoader;

/**
 *
 * @author Andrea
 */
public class MockExchangeRateLoader implements ExchangeRateLoader{

    @Override
    public ExchangeRate load(Currency currency, Currency toCurrency) {
        return new ExchangeRate(1.54);
    }
    
}
