/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Application;

import UI.MoneyCalculatorGUI;
import UI.swing.SwingMoneyCalculatorGUI;
import control.ExchangeCommand;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import mock.MockCurrencySetLoader;
import mock.MockExchangeRateLoader;
import persistence.CurrencySetLoader;
import model.CurrencySet;
import persistence.ExchangeRateLoader;

/**
 *
 * @author usuario
 */
public class MoneyCalculator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        CurrencySetLoader currenciesLoader = new MockCurrencySetLoader();
        ExchangeRateLoader ratesLoader = new MockExchangeRateLoader();
        CurrencySet currencies = new CurrencySet();
        currenciesLoader.load(currencies);
        SwingMoneyCalculatorGUI gui = new SwingMoneyCalculatorGUI(currencies);
        final ExchangeCommand exchange = new ExchangeCommand(gui, currencies, ratesLoader);
        gui.setButtonBehaviour(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                exchange.execute();
            }
        
        });
    }
    
}
